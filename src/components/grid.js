import styled from 'styled-components';

const media = {
  xs: (styles) => `
    @media only screen and (max-width: 480px) {
      ${styles}
    }
  `
};

export const Grid = styled.div`
  border: 5px solid red;
`;

export const Row = styled.div`
  display: flex;
  border: 5px solid blue;
`;

export const Column = styled.div`
  flex: ${(props) => props.size};
  ${(props) => props.collapse && media[props.collapse](`
    display: none;
  `)};
  border: 5px solid green;
`;
