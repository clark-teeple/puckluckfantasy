import React from 'react';
import styled from 'styled-components';

// content
import MainContentWrapper from 'content/main/MainContentWrapper';

// components
import {
  Grid,
  Row
} from 'components/grid';

// styled components
const WindowStyleWrapper = styled.div`
  min-height: 100vh;
`;

const NavStyleWrapper = styled.div`
  background: white;
  height: 10vh;
`;

const ContentStyleWrapper = styled.div`
  background: black;
  color: white;
  height: 90vh;
`;

const FooterStyleWrapper = styled.div`
  background: white;
  height: 20vh;
`;

function App() {
  return (
    <WindowStyleWrapper>
      <NavStyleWrapper>
        <Grid>
          <Row>
            This is nav.
          </Row>
        </Grid>
      </NavStyleWrapper>
      <ContentStyleWrapper>
        <MainContentWrapper />
      </ContentStyleWrapper>
      <FooterStyleWrapper>
        adsfasdfasdf
      </FooterStyleWrapper>
    </WindowStyleWrapper>
  );
}

export default App;
