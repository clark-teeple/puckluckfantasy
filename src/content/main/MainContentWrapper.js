import React, { useEffect, useState } from 'react';
import axios from 'axios';

// components
import {
  Grid,
  Column,
  Row
} from 'components/grid';

function MainContentWrapper() {
  const [utilityData, setUtilityData] = useState([]);
  const [utilityError, setUtilityError] = useState([]);
  const [currentTableData, setCurrentTableData] = useState([]);

  const fetchUtility = async () => {
    await axios.get('https://s3-us-west-2.amazonaws.com/drexsitebucket/utility.csv')
      .then((res) => {
        setUtilityData(res.data);
        // run player filter function
        setCurrentTableData(res.data);
      })
      .catch((error) => {
        setUtilityError(error);
      });
  };

  useEffect(() => {
    fetchUtility();
  }, []);
  return (
    <Grid>
      <Row>
        <Column size={1} collapse="xs">
          asdfasdfasdf
        </Column>
        <Column size={2}>
          eriririririririr
        </Column>
      </Row>
    </Grid>
  );
}

export default MainContentWrapper;
