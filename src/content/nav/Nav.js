import React from 'react';

const Nav = (props) => {
  const {
    whatever
  } = props;
  return (
    <div>
      {whatever}
    </div>
  );
};

Nav.propTypes = {
  whatever: React.propTypes.bool
};

Nav.defaultProps = {
  whatever: true
};

export default Nav;
